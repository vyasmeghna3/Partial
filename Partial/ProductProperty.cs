﻿/* ******************************************************************************************************************************************************
 * Project Name:    Partial
 * Author:          Meghna Vyas(MV)
 * 
 * File:            ProductProperty.cs
 * 
 * Description:     
 *   A C# program to implement buy & selling of products using partial class.
 *   
 * Revision History:   
 *  2018-Jun-15 : Class created (MV)
******************************************************************************************************************************************************* */

/*
 * System class is used
 * */
using System;

namespace Partial
{
    /*A partial class to store product properties.
     * Contains fields with corresponding properties:
     * int id -- holds id of product
     * string name -- holds name of product
     * int stock -- holds stock quantity of product
     * double unitPrice -- holds the price for single unit of product
     * */
    public partial class Product
    {
        int id;
        public int ID { private set { id = value; } get { return id; } }
        string name;
        public string Name { set { name = value; } get { return name; } }
        int stock;
        public int Stock { set { stock = value; } get { return stock; } }
        double unitPrice;
        public double UnitPrice { set { unitPrice = value; } get { return unitPrice; } }
    }
}
