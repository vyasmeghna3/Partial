﻿/* ******************************************************************************************************************************************************
 * Project Name:    Partial
 * Author:          Meghna Vyas(MV)
 * 
 * File:            Product.cs
 * 
 * Description:     
 *   A C# program to implement buy & selling of products using partial class.
 *   
 * Revision History:   
 *  2018-Jun-15 : Class created (MV)
******************************************************************************************************************************************************* */

/*
 * System class is used
 * */
using System;

namespace Partial
{
    class Program
    {
       /*Main class to purchase & sell products
        * */
        static void Main(string[] args)
        {
            Product pro = new Product(1, "NoteBook", 50.0, 25);
            pro.Sell(10);
            pro.Print();
            pro.Sell(20);
        }
    }
}
