﻿/* ******************************************************************************************************************************************************
 * Project Name:    Partial
 * Author:          Meghna Vyas(MV)
 * 
 * File:            ProductMethod.cs
 * 
 * Description:     
 *   A C# program to implement buy & selling of products using partial class.
 *   
 * Revision History:   
 *  2018-Jun-15 : Class created (MV)
******************************************************************************************************************************************************* */

/*
 * System namespace is used
 * */
using System;

namespace Partial
{
    /*partial class that contains methods:
     *  void Purchase() -- purchases a quantity of product and increases the no. of stock by the same no.
     *  void  Sell() -- checks if required quantity of product is in stock and reflects the same on no. the stock
     *  void Print() -- prints the product details
     */
    partial class Product
    {
        /* class constructor to set values of fields
         * */
        public Product( int i, string n, double up, int s=0 )
        {
            id = i;
            name = n;
            stock = s;
            unitPrice = up;

        }

       public void Purchase(int quantity)
        {
            stock += quantity;
        }

       public void Sell(int quantity)
        {
            if (stock >= quantity)
                stock -= quantity;
            else
                Console.WriteLine("Product OUT OF STOCK!");
        
        }

        public void Print()
        {
            Console.WriteLine("Product id = {0}", id);
            Console.WriteLine("Name = {0}", name);
            Console.WriteLine("Stock unit(s) = {0}", stock);
            Console.WriteLine("Unit Price = Rs.{0}\n", unitPrice);

        }
    }
}
